import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_user_exist(host):
    user = host.user("nextcloud")
    assert user.exists
    assert user.uid == 3600
    assert user.gid == 3600


def test_group(host):
    assert host.group("operators").exists
    assert host.group("operators").gid == 3600


def test_nextcloud_client_is_installed(host):
    nextcloud_client = host.package("nextcloud-client")
    assert nextcloud_client.is_installed


def test_gdm_service_enabled(host):
    service = host.service("gdm.service")
    assert service.is_enabled
    assert service.is_running
