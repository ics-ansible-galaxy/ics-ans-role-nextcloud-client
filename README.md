# ics-ans-role-nextcloud-client

Ansible role to install nextcloud-client. Will use gnome keyring for GUI login due to CLI client sync being unreliable. During keyring set password prompt for 'default' keyring, leave it empty.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
---
nextcloud_client_pkg:
  - openssh-server
  - xfce4-panel
  - xfce4-session
  - xfce4-settings
  - xfce4-terminal
  - xfce4-panel
  - xfce4-appfinder
  - xfwm4
  - gdm
  - xorg-x11-drivers
  - gnome-keyring
  - libsecret
  - libgnome-keyring
  - nextcloud-client
  - nfs-utils
nextcloud_client_uid: 3600
nextcloud_client_gid: 3600
nextcloud_client_user: nextcloud
nextcloud_client_group: operators
nextcloud_client_password: dummy
nextcloud_client_nfs: {}
#  nextcloud_client_nfs:
#    src: lcr-share:/exports/LCR
#    mountpoint: /home/nextcloud/Nextcloud
#    opts: ro

# Variables to make role re-usable for lcr-ws10
nextcloud_client_user_and_group_enable: true
nextcloud_client_autologin: true
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nextcloud-client
```

## License

BSD 2-clause
